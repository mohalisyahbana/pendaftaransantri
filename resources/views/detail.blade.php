@extends('layout.adminserver')
@push('css6')
    <!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@section('content')

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Info santri</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Data Info santri</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <div class="card-body">
            <table class="table table-bordered">
            <div class="row">
                <table class="table">
                    <thead>
                      <tr>

                        <th scope="col">Nomer</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">No.Handphone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Dibuat</th>
                        {{-- <th scope="col">Aksi</th> --}}
                      </tr>
                    </thead>
                    <tbody>
                    {{-- @php
                        $no = 1;
                    @endphp --}}
                    {{-- @foreach ($data as $index => $row) --}}
                    <tr>
                        <td>1</td>
                        <td>{{  $detail->nama  }}</td>
                        <td>
                            <img src="{{ asset('fotoklien/'.$detail->foto) }}" alt="" style="width: 40px">
                        </td>
                        <td>{{  $detail->jeniskelamin  }}</td>
                        <td>0{{ $detail->nohandphone }}</td>
                        <td>{{ $detail->email }}</td>
                        <td>{{ $detail->created_at->format('D M Y') }}</td>
                        <td>

                        </td>
                      </tr>
                      <a href="/klien" class="btn btn-success">Keluar</a>
                </div>
                </tbody>
            <table>
        <div>
        {{-- <!...card-body --> --}}
    </div>
@endsection


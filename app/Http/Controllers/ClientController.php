<?php

namespace App\Http\Controllers;


use App\Models\user;
use App\Models\Client;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
// use Symfony\Component\HttpFoundation\RequestStack;

class ClientController extends Controller
{
    public function index(Request $Request){

        if($Request->has('search')){
            $data = Client::where('nama','LIKE','%' .$Request->search.'%')->paginate(5);
        }else{
            $data = Client::paginate(5);
        }

        return view('dataklien',compact('data'));
    }

    public function tambahklien(){
        return view('tambahdata');
    }

    public function insertdata(Request $request){

        $data = Client::create($request->all());
        if($request->hasFile('foto')){
            $request->file('foto')->move('fotoklien/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('klien')->with('success',' Data Berhasil Disimpan ');
    }

    public function tampilkandata($id){
        $data = Client::find($id);
        return view('tampildata', compact('data'));
    }

    public function updatedata(Request $request,$id){
        $data = Client::find($id);
        $data->update($request->all());
        return redirect()->route('klien')->with('success',' Data Berhasil Diupdate ');
    }

    public function delete($id){
        $data = Client::find($id);
        $data->delete();
        return redirect()->route('klien')->with('success',' Data Berhasil DiHapus ');
    }

    public function detail($id){
        $detail = Client::find($id);
        return view('detail',compact('detail'));
    }

    public function exportpdf(){
        $data = client::all();

        view()->share('data', $data);
        $pdf = PDF::loadview('dataklien-pdf');
        return $pdf->download('data.pdf');
    }

    public function user(){
        $data = User::all();
        return view('user', compact('data'));
    }

    public function deletuser($id)
    {
        $data = user::find($id);
        $data->delete();
        return redirect()->route('user');
    }

}

